#include <iostream>
#include <string>
#include <iomanip>

int main()
{
	std::cout << "Welcome To Calculator!!!" << std::endl << "Insert Your Phrase:";
	std::string str1, str2, str3;
	std::cin >> str1 >> str2 >> str3;
	bool exit = false;
	if(1 != str2.length())
	{
		std::cout << "operand can be only one char" << std::endl;
		exit = true;
	}
	else
	{
		char *sz1[ 1 ], *sz2[ 1 ];
		double num1 = std::strtod(str1.c_str(), sz1),
			num2 = std::strtod(str3.c_str(), sz2),
			result;
		if(*sz1 == str1.c_str() || *sz2 == str3.c_str())
		{
			std::cout << "can't convert input numbers" << std::endl;
			exit = true;
		}
		else
		{
			switch(str2[ 0 ])
			{
				case '+':
					result = num1 + num2;
					break;
				case '-':
					result = num1 - num2;
					break;
				case '/':
				case '\\':
					if(std::abs(num2) < std::numeric_limits<double>::epsilon()) //num2 == 0
					{
						std::cout << "Dividing By Zero!!!" << std::endl;
						exit = true;
					}
					else
					{
						result = num1 / num2;
					}
					break;
				case '*':
				case 'X':
				case 'x':
					result = num1 * num2;
					break;
				default:
					std::cout << "Unknowon operand" << std::endl;
					exit = true;
			}
			if(!exit)
			{
				std::cout << num1 << " " << str2 << " " << num2 << " = " << result << std::endl;
			}
		}
	}
	
	return exit;
}